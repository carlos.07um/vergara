/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.quakereport;

import java.text.DecimalFormat;

/**
 * An {@link Earthquake} object contains information related to a single earthquake.
 */
public class Earthquake {


    public long id;
    public long idRutina;
    public String img;
    public String name;


    public Earthquake(String name, String img) {
        this.name = name;
        this.img = img;
        id = 0;
        idRutina = 0;
    }

    public String getID(){
        DecimalFormat decimalFormat = new DecimalFormat("#");
        String numberAsString = decimalFormat.format(id);
        return numberAsString;
    }

    public String getIDRutina(){
        DecimalFormat decimalFormat = new DecimalFormat("#");
        String numberAsString = decimalFormat.format(idRutina);
        return numberAsString;
    }

}