package com.example.android.quakereport;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by carlos on 11/05/17.
 */
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button crearRutinaButton = (Button) findViewById(R.id.createRutineButton);
        crearRutinaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i;
                i = new Intent(getApplicationContext(), EarthquakeActivity.class);
                startActivity(i);
            }
        });

        Button misRutinasButton = (Button) findViewById(R.id.rutinasButton);
        misRutinasButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i;
                i = new Intent(getApplicationContext(), RutinasActivity.class);
                startActivity(i);
            }
        });
    }
}
