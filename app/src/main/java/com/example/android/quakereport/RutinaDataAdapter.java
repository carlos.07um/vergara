package com.example.android.quakereport;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by carlos on 10/05/17.
 */
public class RutinaDataAdapter {

    private static final String DATABASE_NAME = "yoga.db";
    private static final String TABLE_NAME = "rutinas";
    private static final int DATABASE_VERSION = 1;

    public static final String KEY_ID = "_id";
    public static final String KEY_NAME = "name";

    private static String DROP_STATEMENT = "DROP TABLE IF EXISTS " + TABLE_NAME;
    private static String CREATE_STATEMENT;
    static {
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE " + TABLE_NAME + " (");
        sb.append(KEY_ID + " integer primary key autoincrement, ");
        sb.append(KEY_NAME + " text");
        sb.append(")");
        CREATE_STATEMENT = sb.toString();
    }

    private SQLiteOpenHelper mOpenHelper;
    private SQLiteDatabase mDatabase;

    public RutinaDataAdapter(Context context){
        mOpenHelper = new RutinaDBHelper(context);
    }

    public void open(){
        mDatabase = mOpenHelper.getWritableDatabase();
    }

    public void close(){
        mDatabase.close();
    }


    public long addRutina(Rutina rutina){

        ContentValues row = getContentValuesFromRutina(rutina);

        long newid = mDatabase.insert(TABLE_NAME, null, row);

        rutina.dbID = newid;

        return newid;
    }

    private ContentValues getContentValuesFromRutina(Rutina rutina) {
        ContentValues row = new ContentValues();

        row.put(KEY_NAME, rutina.name);

        return row;
    }

    public Cursor getRutinasCursor(){
        String[] rows_i_want =  new String[]{KEY_ID, KEY_NAME};
        return mDatabase.query(TABLE_NAME, rows_i_want, null, null, null, null, KEY_ID + " DESC ");
    }

    private static class RutinaDBHelper extends SQLiteOpenHelper {

        public RutinaDBHelper(Context context) {
            super(context, DATABASE_NAME, null,DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_STATEMENT);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.d(EarthquakeActivity.SLS, "Upgrading the db from version " + oldVersion + " to " + newVersion);
            db.execSQL(DROP_STATEMENT);
            onCreate(db);
        }
    }

}
