package com.example.android.quakereport;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by carlos on 10/05/17.
 */
public class EjerciciosDataAdapter {

    private static final String DATABASE_NAME = "yoga.db";
    private static final String TABLE_NAME = "ejercicios";
    private static final int DATABASE_VERSION = 1;

    public static final String KEY_ID = "_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_IMG = "img";
    public static final String KEY_ID_RUTINA = "idRutina";

    private static String DROP_STATEMENT = "DROP TABLE IF EXISTS " + TABLE_NAME;
    private static String CREATE_STATEMENT;
    static {
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE " + TABLE_NAME + " (");
        sb.append(KEY_ID + " integer primary key autoincrement, ");
        sb.append(KEY_NAME + " text, ");
        sb.append(KEY_IMG + " text, ");
        sb.append(KEY_ID_RUTINA + " integer ");
        sb.append(")");
        CREATE_STATEMENT = sb.toString();
    }

    private SQLiteOpenHelper mOpenHelper;
    private SQLiteDatabase mDatabase;

    public EjerciciosDataAdapter(Context context){
        mOpenHelper = new EjercicioDBHelper(context);
    }

    public void open(){
        mDatabase = mOpenHelper.getWritableDatabase();
    }

    public void close(){
        mDatabase.close();
    }


    public long addEjercicio(Earthquake ejercicio){

        ContentValues row = getContentValuesFromEjercicio(ejercicio);

        long newid = mDatabase.insert(TABLE_NAME, null, row);

        ejercicio.id = newid;

        return newid;
    }

    private ContentValues getContentValuesFromEjercicio(Earthquake ejercicio) {
        ContentValues row = new ContentValues();

        row.put(KEY_NAME, ejercicio.name);
        row.put(KEY_IMG, ejercicio.img);
        row.put(KEY_ID_RUTINA, ejercicio.idRutina);

        return row;
    }

    public Cursor getEjerciciosCursor(long idRutina){
        //String[] rows_i_want =  new String[]{KEY_ID, KEY_NAME};
        return mDatabase.query(TABLE_NAME, null, " WHERE idRutina = " + Long.toString(idRutina), null, null, null, KEY_ID + " DESC ");
        //return mDatabase.query(TABLE_NAME, rows_i_want, null, null, null, null, KEY_ID + " DESC ");
    }

    private static class EjercicioDBHelper extends SQLiteOpenHelper {

        public EjercicioDBHelper(Context context) {
            super(context, DATABASE_NAME, null,DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_STATEMENT);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.d(EarthquakeActivity.SLS, "Upgrading the db from version " + oldVersion + " to " + newVersion);
            db.execSQL(DROP_STATEMENT);
            onCreate(db);
        }
    }

}
