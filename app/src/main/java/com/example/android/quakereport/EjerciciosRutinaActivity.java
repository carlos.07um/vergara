package com.example.android.quakereport;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by carlos on 11/05/17.
 */
public class EjerciciosRutinaActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ejercicios_activity);

        Bundle bundle = getIntent().getExtras();
        long id = bundle.getLong("idRutina");

        TextView texto = (TextView) findViewById(R.id.idTextView);
        texto.setText("El id recuperado es " + Long.toString(id));
    }
}
