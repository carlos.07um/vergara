package com.example.android.quakereport;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by carlos on 11/05/17.
 */
public class RutinasActivity extends AppCompatActivity {

    private RutinaDataAdapter mRutinaDataAdapter;
    private SimpleCursorAdapter mCursorAdapter;
    public static final String SLS = "SLS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rutinas_activity);


        mRutinaDataAdapter = new RutinaDataAdapter(this);
        mRutinaDataAdapter.open();

        Cursor cursor = mRutinaDataAdapter.getRutinasCursor();
        String[] fromColumns = new String[]{RutinaDataAdapter.KEY_ID, RutinaDataAdapter.KEY_NAME};
        int[] toTextViews  = new int[]{R.id.textViewID, R.id.textViewName};
        mCursorAdapter = new SimpleCursorAdapter(this, R.layout.mis_rutinas_item, cursor, fromColumns, toTextViews, 0);

        final ListView rutinasListView = (ListView) findViewById(R.id.listMisRutinas);
        rutinasListView.setAdapter(mCursorAdapter);

        rutinasListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //View rutinaItem = rutinasListView.getItemAtPosition(position);

                Cursor cursor = (Cursor) parent.getAdapter().getItem(position);

                //1 es el nombre
                //0 es el id
                Toast.makeText(getApplicationContext(), "Rutina elegida: " + cursor.getString(1) + "  " + cursor.getLong(0),
                        Toast.LENGTH_LONG).show();

                Intent i;
                i = new Intent(getApplicationContext(), EjerciciosRutinaActivity.class);
                i.putExtra("idRutina", cursor.getLong(0));
                startActivity(i);
            }
        });
    }
}
