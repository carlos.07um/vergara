/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.quakereport;

import android.app.Dialog;
import android.app.LoaderManager;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class EarthquakeActivity extends AppCompatActivity
        implements LoaderCallbacks<List<Earthquake>> {

    public static final String SLS = "SLS";
    private RutinaDataAdapter mRutinaDataAdapter;
    private EjerciciosDataAdapter mEjercicioDataAdapter;

    private SimpleCursorAdapter mCursorAdapter;

    private static final String LOG_TAG = EarthquakeActivity.class.getName();

    private static final String USGS_REQUEST_URL =
            "https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&orderby=time&minmag=6";

    /**
     * Constant value for the earthquake loader ID. We can choose any integer.
     * This really only comes into play if you're using multiple loaders.
     */
    private static final int EARTHQUAKE_LOADER_ID = 1;

    private EarthquakeAdapter mAdapter;

    private ArrayList<Earthquake> ejerciciosArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.earthquake_activity);

        mRutinaDataAdapter = new RutinaDataAdapter(this);
        mRutinaDataAdapter.open();

        mEjercicioDataAdapter = new EjerciciosDataAdapter(this);
        mEjercicioDataAdapter.open();

        final GridView earthquakeListView = (GridView) findViewById(R.id.grid);

        mAdapter = new EarthquakeAdapter(this, new ArrayList<Earthquake>());

        earthquakeListView.setAdapter(mAdapter);

        ejerciciosArray = new ArrayList<Earthquake>();

        final Button saveRutineButton = (Button) findViewById(R.id.buttonClick);
        saveRutineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                final Dialog dialog = new Dialog(EarthquakeActivity.this);

                dialog.setContentView(R.layout.dialog_signin);

                dialog.show();

                Button confirmButton = (Button) dialog.findViewById(R.id.confirmButton) ;
                confirmButton.setOnClickListener(new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {

                        EditText nombreRutinaEditText = (EditText) dialog.findViewById(R.id.nameRutineEditText);
                        String nombreRutina = nombreRutinaEditText.getText().toString();

                        if (nombreRutina.matches("")) {
                            Toast.makeText(getApplication(), "Favor de ingresar un identificador", Toast.LENGTH_LONG).show();
                            return;
                        }
                        else{

                            Rutina newRutine = new Rutina(nombreRutina, -666);

                            addRutina(newRutine);

                            setIDRutinaToEjercicio(newRutine.dbID);

                            addEjercicios();

                            String cadena = "rutinaName= " + newRutine.name + " rutinaID= " + newRutine.getDBID() + "\n\n";

                            cadena += cadenaEjerciciosRegistrados();

                            Toast.makeText(getApplicationContext(), cadena ,
                                    Toast.LENGTH_LONG).show();

                            Intent i;
                            i = new Intent(getApplicationContext(), RutinasActivity.class);
                            startActivity(i);

                            dialog.dismiss();
                        }
                    }
                });

                Button declineButton = (Button) dialog.findViewById(R.id.declineButton);
                // if decline button is clicked, close the custom dialog
                declineButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Close dialog
                        dialog.dismiss();
                    }
                });
            }
        });

        earthquakeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                //Earthquake currentEarthquake = mAdapter.getItem(position);
                Earthquake currentEarthquake = (Earthquake) earthquakeListView.getItemAtPosition(position);
                //earthquakeListView.getSelectedView().setBackgroundColor(getResources().getColor(R.color.selected));

                if(ejerciciosArray.contains(currentEarthquake)){
                    //quitar selección y borrar
                    mAdapter.getView(position, view, adapterView).setBackgroundColor(getResources().getColor(R.color.deselected));
                    //earthquakeListView.getChildAt(position)

                    ejerciciosArray.remove(currentEarthquake);
                }
                else{
                    //seleccionar y agregar
                    ejerciciosArray.add(currentEarthquake);
                    mAdapter.getView(position, view, adapterView).setBackgroundColor(getResources().getColor(R.color.selected));
                }

                if(ejerciciosArray.isEmpty()){
                    saveRutineButton.setVisibility(View.GONE);
                }
                else{
                    saveRutineButton.setVisibility(View.VISIBLE);
                }

            }
        });

        // Get a reference to the LoaderManager, in order to interact with loaders.
        LoaderManager loaderManager = getLoaderManager();

        loaderManager.initLoader(EARTHQUAKE_LOADER_ID, null, this);
    }

    private void addRutina(Rutina rutina){
        mRutinaDataAdapter.addRutina(rutina);
    }

    private void addEjercicios(){

        for(int i = 0; i<ejerciciosArray.size(); i++) {
            mEjercicioDataAdapter.addEjercicio(ejerciciosArray.get(i));
        }

    }

    private String cadenaEjerciciosRegistrados(){
        String cadena = "";

        for(int i = 0; i<ejerciciosArray.size(); i++) {
            Earthquake e = ejerciciosArray.get(i);
            cadena += "idE: " + e.getID() + " nombreE: " + e.name + " img: " + e.img + " idRutina: " + e.getIDRutina() + "\n";
        }

        return cadena;
    }

    private void setIDRutinaToEjercicio(long idRutina){
        for(int i = 0; i<ejerciciosArray.size(); i++){
            ejerciciosArray.get(i).idRutina = idRutina;
        }
    }

    @Override
    public Loader<List<Earthquake>> onCreateLoader(int i, Bundle bundle) {
        // Create a new loader for the given URL
        return new EarthquakeLoader(this, USGS_REQUEST_URL);
    }

    @Override
    public void onLoadFinished(Loader<List<Earthquake>> loader, List<Earthquake> earthquakes) {
        // Clear the adapter of previous earthquake data
        mAdapter.clear();

        // If there is a valid list of {@link Earthquake}s, then add them to the adapter's
        // data set. This will trigger the ListView to update.
        if (earthquakes != null && !earthquakes.isEmpty()) {
            mAdapter.addAll(earthquakes);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Earthquake>> loader) {
        // Loader reset, so we can clear out our existing data.
        mAdapter.clear();
    }
}